import java.io.*;
import java.util.*;

public class EnglishDB {

    public Map<String, String[]> db = new HashMap<>();
    public Map<Integer, String> keys = new HashMap<>();

    private Statistic statistic;

    private String currentWord = null;
    private String[] currentTranslation = null;
    private String[] history = new String[20];

    /*private int[] status = new int[2];
    private int blockID = 1;
    private int blockSize = 10;*/
    private int historyCount = 0;

    public EnglishDB() throws IOException {
        String[] word;

        try (BufferedReader br = new BufferedReader(new FileReader("/home/maldan/Documents/notes/english_db.txt")))
        {
            String sCurrentLine;
            int pos = 0;

            while ((sCurrentLine = br.readLine()) != null) {
                /** Parse words */
                word = sCurrentLine.split(" - ");
                if (word.length < 2) continue;

                /** Parse parts */
                String[] parts = word[1].split(",");

                for (int i = 0; i < parts.length; i++) {
                    parts[i] = parts[i].trim();
                }

                /** Add to db */
                db.put(word[0], parts);
                keys.put(pos++, word[0]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        /** Show total vocabulary size */
        System.out.println(db.size());
    }

    public void showHistory() {
        for (String hist:history) {
            if (hist == null) continue;
            if (hist.equals("")) continue;
            System.out.println(hist);
        }
    }

    public void loadStat() throws Exception {
        FileInputStream fis = null;
        ObjectInputStream oin = null;

        try {
            fis = new FileInputStream("stat.data");
            oin = new ObjectInputStream(fis);
        }
        catch (Exception e) {
            System.out.println("load error");
        }

        try {
            this.statistic = (Statistic) oin.readObject();
        }
        catch (Exception e) {
            this.statistic = new Statistic();
            System.out.println("parse error");
        }
    }

    public void saveStat() throws Exception {
        FileOutputStream fos = new FileOutputStream("stat.data");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(statistic);
        oos.flush();
        oos.close();
    }

    public void blockOffset(int offset) {
        this.statistic.blockID += offset;
    }

    public String getWordData() {
        return "> " + Main.ANSI_YELLOW + this.currentWord + Main.ANSI_RESET + " ";
    }

    public void showCorrect() {
        for (int i = 0; i < currentTranslation.length; i++) {
            System.out.print(currentTranslation[i]);

            if (i < currentTranslation.length - 1) System.out.print(", ");
        }

        System.out.println();
    }

    public void showCurrentStatus() {
        System.out.println("###########################################");
        System.out.println("# Correct: " + Main.ANSI_GREEN + this.statistic.correct +
                Main.ANSI_RESET + " / Wrong: " +
                Main.ANSI_RED + this.statistic.wrong + Main.ANSI_RESET);

        System.out.print("# Current words: ");

        System.out.print(Main.ANSI_CYAN);
        for (int i = 0; i < this.statistic.blockSize; i++) {
            int posid = this.statistic.blockID * this.statistic.blockSize + i;
            System.out.print(keys.get(posid));
            if (i < this.statistic.blockSize - 1) System.out.print(", ");
        }
        System.out.print(Main.ANSI_RESET);

        System.out.println();
        System.out.println();
    }

    public boolean checkWord(String input) {
        int pos = java.util.Arrays.asList(this.currentTranslation).indexOf(input);

        history[historyCount] = "> " + historyCount + ": " + Main.ANSI_GREEN +
                this.currentWord + Main.ANSI_RESET + " " + input;

        historyCount++;

        if (historyCount >= 10) {
            for (int i = 0; i < history.length; i++) {
                history[i] = "";
            }

            historyCount = 0;
        }

        float value = 0;

        if (this.statistic.wordstat.get(this.currentWord) != null)
            value = this.statistic.wordstat.get(this.currentWord);

        if (pos != -1) value += 0.2f;
        else value -= 0.35f;

        if (value <= 0) value = 0;

        this.statistic.wordstat.put(this.currentWord, value);

        if (pos == -1) {
            this.statistic.wrong += 1;
            return false;
        } else {
            this.statistic.correct += 1;
            return true;
        }
    }

    public void setWrong() {
        this.statistic.wrong += 1;

        try {
            this.saveStat();
        }
        catch (Exception e) {

        }
    }

    public void createRandomWord() {
        int position = (this.statistic.blockID * this.statistic.blockSize);
                // + (int)(Math.random() * this.statistic.blockSize);

        HashMap<String, Float> tmpTable = new HashMap<>();
        String first = "";
        float value = 0;

        for (int i = 0; i < this.statistic.blockSize; i++) {
            String word = this.keys.get((this.statistic.blockID * this.statistic.blockSize) + i);

            if (!word.isEmpty()) {
                if (this.statistic.wordstat.get(word) != null)
                    tmpTable.put(word, this.statistic.wordstat.get(word));
                else
                    tmpTable.put(word, 0.0f);
            }
        }

        tmpTable = (HashMap<String, Float>)sortByValue(tmpTable);

        for(Map.Entry<String, Float> entry : tmpTable.entrySet()) {
            first = entry.getKey();
            value = entry.getValue();

            if (Math.random() >= 0.15) break;
            else continue;
        }

        if (value >= 1) {
            System.out.println("END!!");
            this.statistic.blockID += 1;
            this.createRandomWord();
            return;
        }

        System.out.println(tmpTable);
        System.out.println();

        this.currentWord = first;
        this.currentTranslation = db.get(this.currentWord);
    }

    static Map sortByValue(Map map) {
        List list = new LinkedList(map.entrySet());
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        Map result = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry)it.next();
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
}
