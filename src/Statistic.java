import java.io.Serializable;
import java.util.HashMap;

public class Statistic implements Serializable {
    public int blockID = 0;
    public int blockSize = 10;
    public int correct = 0;
    public int wrong = 0;

    public HashMap<String, Float> wordstat = new HashMap<>();

}
