import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static void main(String[] argv) throws Exception {
        EnglishDB db = new EnglishDB();

        db.loadStat();

        while (true) {
            System.out.print("\033[H\033[2J");

            /** show current statw */
            db.showCurrentStatus();

            /** show current word */
            db.createRandomWord();
            db.showHistory();
            System.out.print(db.getWordData());

            /** prompt answer */
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String readWord = bufferRead.readLine();

            /** check alternative commands */
            if (readWord.equals("хз")) {
                db.showCorrect();
                db.setWrong();
                bufferRead.readLine();
            } else if (readWord.equals("+")) {
                db.blockOffset(1);
            } else if (readWord.equals("-")) {
                db.blockOffset(-1);
            } else {
                db.checkWord(readWord);
            }

            db.saveStat();
        }
    }
}
